# Kwik

This is just some project, keep going mate! Nothing to see here (:

#Instructions

To use it, just go to ../data/cart.yml and put whatever products you wish in whatever order you wish from its code.
Then execute ../lib/kwik.rb and you'll get the total price of the products.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
