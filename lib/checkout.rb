class Checkout

  def initialize(discount_rules, prods)
    @cart = Hash.new
    @discount = discount_rules
    @products = prods

    @total = -1
  end

  def scan(item)
    _exists = @products.any?{ |p| p['code'] == item }

    if _exists
      @cart[item] = 0 if @cart[item].nil?
      @cart[item] += 1
    end

    return @cart
  end

  def total
    _price = 0
    @cart.keys.each_with_index do |key, i|
      _price += get_price_by_key(key)
    end

    return _price
  end

  private

  def get_price_by_key(key)

    _price = 0
    _product = @products.detect {|e| e['code'] == key}

    #Check for discounts
    _discount = @discount.detect {|e| e['code'] == key}
    if !_discount.nil? && @cart[key] >= _discount['starts_at']
      case _discount['type']
        when '2x1'
          _price = (@cart[key] / 2) * _product['price']
          _price += _product['price'] * (@cart[key] % 2)

        when 'individual-drop'
          _price = @cart[key] * (_product['price'] * (1 - _discount['amount']))

        when 'global-percent'
          _price = (@cart[key] * _product['price']) * (1 - _discount['amount'])
      end
    else
      #No discount, go with full price
      _price = _product['price'] * @cart[key]
    end

    return _price.round(2)

  end


end
