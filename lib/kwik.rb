require_relative "loader"
require_relative "checkout"
require_relative "kwik/version"

module Kwik
  class Error < StandardError; end

  #Load everything
  _loader = Loader.new
  _products = _loader.load_products
  _discounts = _loader.load_discount_rules
  _cart = _loader.load_cart

  #Init checkout machine
  _checkout = Checkout.new(_discounts, _products)

  #Scan cart
  _cart.each do |i|
    _checkout.scan(i)
  end

  #Return total
  p "The total of your purchase is #{_checkout.total}"
end
