require 'pathname'

module Kwik
  VERSION = "0.1.0"
  ROOT = Pathname.new(File.expand_path(__FILE__)).parent.parent.parent.to_s
end
