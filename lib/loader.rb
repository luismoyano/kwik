require 'yaml'
require_relative 'kwik/version'

class Loader

  def initialize
    @products_path = File.expand_path(Kwik::ROOT + '/data/products.yml')
    @discounts_path = File.expand_path(Kwik::ROOT + '/data/discount_rules.yml')
    @kwik_cart_path = File.expand_path(Kwik::ROOT + '/data/cart.yml')
  end

  def set_products_path(path)
    @products_path = path
  end

  def set_discounts_path(path)
    @discounts_path = path
  end

  def load_products
    @products = YAML.load_file(@products_path)
    validate_data(@products)
    return @products

  end

  def load_discount_rules
    @rules = YAML.load_file(@discounts_path)
    #Validate the presence of the data
    validate_data(@rules)

    #validate the discounts exist for an existing product
    validate_discounts(@rules)
    return @rules
  end

  def load_cart
    @kwik_cart = YAML.load_file(@kwik_cart_path)
    return @kwik_cart

  end

  private

  def validate_data(data)
    data.each do |p|
      p.each do |key, value|
        if !value || value.is_a?(String) && value.empty? #rails blank accounting for non string values
          raise 'A value is missing, please review your data'
        end
      end
    end
  end

  def validate_discounts(discounts)
    load_products if @products.nil?

    _code_exists = false

    discounts.each do |d|
      _code_exists = @products.any? do |item|
        item['code'] == d['code']
      end
    end
    raise 'The introduced offer doesnt have a product associated' unless _code_exists
  end

end
