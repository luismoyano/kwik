RSpec.describe Checkout do

  context 'when scanning existing items' do
    let(:green_tea) { 'GR1' }
    let(:strawberries) { 'SR1' }
    let(:coffee) { 'CF1' }

    let(:loader) { Loader.new }
    let(:prods) { loader.load_products }
    let(:discounts) { loader.load_discount_rules }

    let(:checkout) { Checkout.new(discounts, prods) }

    it 'checks the items correctly' do
      _cart = checkout.scan(green_tea)
      expect(_cart[green_tea]).to eq(1)

      _cart = checkout.scan(strawberries)
      expect(_cart[strawberries]).to eq(1)

      _cart = checkout.scan(coffee)
      expect(_cart[coffee]).to eq(1)
    end
  end

  context 'when scanning non existing items' do
    let(:red_tea) { 'RT1' }
    let(:cranberries) { 'CR1' }
    let(:toffee) { 'TF1' }

    let(:loader) { Loader.new }
    let(:prods) { loader.load_products }
    let(:discounts) { loader.load_discount_rules }

    let(:checkout) { Checkout.new(discounts, prods) }

    it 'doesnt check the items' do
      _cart = checkout.scan(red_tea)
      expect(_cart[red_tea]).to be(nil)

      _cart = checkout.scan(cranberries)
      expect(_cart[cranberries]).to be(nil)

      _cart = checkout.scan(toffee)
      expect(_cart[toffee]).to be(nil)
    end
  end

  context 'when scanning two of the same item' do
    let(:green_tea) { 'GR1' }

    let(:loader) { Loader.new }
    let(:prods) { loader.load_products }
    let(:discounts) { loader.load_discount_rules }

    let(:checkout) { Checkout.new(discounts, prods) }

    it 'checks the items correctly' do
      _cart = checkout.scan(green_tea)
      _cart = checkout.scan(green_tea)

      expect(_cart[green_tea]).to eq(2)
    end
  end

  describe 'Requesting the total of a purchase' do
    let(:green_tea) { 'GR1' }
    let(:strawberries) { 'SR1' }
    let(:coffee) { 'CF1' }

    let(:loader) { Loader.new }
    let(:prods) { loader.load_products }
    let(:discounts) { loader.load_discount_rules }

    let(:checkout) { Checkout.new(discounts, prods) }


    context 'Checking the test 1' do
      it 'retrieves the expected price' do
        checkout.scan(green_tea)
        checkout.scan(strawberries)
        checkout.scan(green_tea)
        checkout.scan(green_tea)
        checkout.scan(coffee)


        expect(checkout.total).to eq(22.45)
      end
    end

    context 'Checking the test 2' do
      it 'retrieves the expected price' do
        checkout.scan(green_tea)
        checkout.scan(green_tea)

        expect(checkout.total).to eq(3.11)
      end
    end

    context 'Checking the test 3' do
      it 'retrieves the expected price' do
        checkout.scan(strawberries)
        checkout.scan(strawberries)
        checkout.scan(green_tea)
        checkout.scan(strawberries)


        expect(checkout.total).to eq(16.61)
      end
    end

    context 'Checking the test 4' do
      it 'retrieves the expected price' do
        checkout.scan(green_tea)
        checkout.scan(coffee)
        checkout.scan(strawberries)
        checkout.scan(coffee)
        checkout.scan(coffee)


        expect(checkout.total).to eq(30.57)
      end
    end
  end
end
