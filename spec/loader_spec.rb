RSpec.describe Loader do

  context 'When Loading a products file' do

    let(:loader) { Loader.new }
    let(:products) { loader.load_products }

    it 'keeps all the products on a data structure' do
      expect(products).not_to be(nil)
      expect(products.size).to be > 0
    end
  end

  context 'When Loading a products file with missing codes' do

    let(:loader) { Loader.new }
    let(:missing_codes_path) { File.expand_path(Dir.pwd + '/data/test/no_code_products.yml') }

    it 'It raises an error' do
      loader.set_products_path(missing_codes_path)
      expect{ loader.load_products }.to raise_error(RuntimeError, 'A value is missing, please review your data')
    end
  end

  context 'When Loading a products file with missing names' do

    let(:loader) { Loader.new }
    let(:missing_names_path) { File.expand_path(Dir.pwd + '/data/test/no_name_products.yml') }

    it 'It raises an error' do
      loader.set_products_path(missing_names_path)
      expect{ loader.load_products }.to raise_error(RuntimeError, 'A value is missing, please review your data')
    end
  end

  context 'When Loading a products file with missing prices' do

    let(:loader) { Loader.new }
    let(:missing_prices_path) { File.expand_path(Dir.pwd + '/data/test/no_price_products.yml') }

    it 'It raises an error' do
      loader.set_products_path(missing_prices_path)
      expect{ loader.load_products }.to raise_error(RuntimeError, 'A value is missing, please review your data')
    end
  end

  context 'When Loading a discounts file' do

    let(:loader) { Loader.new }
    let(:rules) { loader.load_discount_rules }

    it 'keeps all the discount rules on a data structure' do
      expect(rules).not_to be(nil)
      expect(rules.size).to be > 0
    end
  end

  context 'When Loading a discount file with non existing products' do

    let(:loader) { Loader.new }
    let(:wrong_product_path) { File.expand_path(Dir.pwd + '/data/test/wrong_product_rules.yml') }

    it 'It raises an error' do
      loader.set_discounts_path(wrong_product_path)
      expect{ loader.load_discount_rules }.to raise_error(RuntimeError, 'The introduced offer doesnt have a product associated')
    end
  end

  context 'When Loading a discount file with missing offer types' do

    let(:loader) { Loader.new }
    let(:nt_product_path) { File.expand_path(Dir.pwd + '/data/test/no_type_product_rules.yml') }

    it 'It raises an error' do
      loader.set_discounts_path(nt_product_path)
      expect{ loader.load_discount_rules }.to raise_error(RuntimeError, 'A value is missing, please review your data')
    end
  end

  context 'When Loading a discount file with missing product codes' do

    let(:loader) { Loader.new }
    let(:n_product_path) { File.expand_path(Dir.pwd + '/data/test/no_product_rules.yml') }

    it 'It raises an error' do
      loader.set_discounts_path(n_product_path)
      expect{ loader.load_discount_rules }.to raise_error(RuntimeError, 'A value is missing, please review your data')
    end
  end
end
